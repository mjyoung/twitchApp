import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    selectCategory(params) {
      let selectedCategory = params;
      this.set('selectedCategory', selectedCategory);
      this.filterGames(selectedCategory);
    }
  },

  filterGames(selectedCategory) {
    let promises = this.gameCategories[selectedCategory].map((game) => {
      return new Ember.RSVP.Promise(function(resolve, reject) {

        let streams;

        return Twitch.api({method: 'streams', params: {game: game, limit:3} }, function(error, list) {
          streams = list.streams.map((stream) => {
            return {
              user: stream.channel.name,
              game: stream.game,
              status: stream.channel.status,
              viewers:stream.viewers,
              preview: stream.preview.medium,
              url: stream.channel.url
            };
          });
          if (error) {
            Ember.run(null, reject, error);
          } else {
            Ember.run(null, resolve, streams);
          }
        });
      });
    });

    Promise.all(promises).then((allFilteredGameStreams) => {
      let streamList = [];
      for (var filteredStreams of allFilteredGameStreams) {
        streamList = streamList.concat(filteredStreams);
      }
      console.log('streamList', streamList);
      this.set('model', streamList);
    });
  },

  categories: [
    'Action', 'Shooter', 'RPG', 'MOBA', 'MMO', 'Strategy', 'Horror', 'Sports', 'Creative'
  ],

  gameCategories: {
    Action: [
      'Grand Theft Auto V',
      'DayZ',
      'Path of Exile'
    ],
    Shooter: [
      'Overwatch',
      'Counter-Strike: Global Offensive',
      'Call of Duty: Black Ops III'
    ],
    RPG: [
      'The Elder Scrolls V: Skyrim',
      'The Witcher 3: Wild Hunt'
    ],
    MOBA: [
      'Dota 2',
      'League of Legends',
      'Heroes of the Storm',
      'Smite'
    ],
    MMO: [
      'World of Warcraft',
      'RuneScape',
      'World of Tanks'
    ],
    Strategy: [
      'Clash Royale',
      'Hearthstone: Heroes of Warcraft',
      'League of Legends',
      'Dota 2'
    ],
    Horror: [
      'Dead by Daylight',
      'Resident Evil 6',
      'Dark Souls III'
    ],
    Sports: [
      'FIFA 16',
      'NBA 2K16'
    ],
    Creative: [
      'Social Eating',
      'Creative',
      'Drawful 2'
    ]
  }
});
