import Model from 'ember-data/model';
import attr from 'ember-data/attr';

export default Model.extend({
  user: attr(),
  game: attr(),
  status: attr(),
  viewers: attr(),
  preview: attr(),
  url: attr()
});
