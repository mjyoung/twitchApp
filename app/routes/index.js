import Ember from 'ember';
import ENV from 'twitch-app/config/environment';

let mockStreams = [
  {
    user: 'imaqtpie',
    game: 'League of Legends',
    status: 'Raise your Dongers',
    viewers: '20543',
    preview: 'https://static-cdn.jtvnw.net/previews-ttv/live_user_c9sneaky-320x180.jpg',
    url: 'https://www.twitch.tv/c9sneaky'
  },
  {
    user: 'a_seagull',
    game: 'Overwatch',
    status: 'Ranked Overwatch, Baby!',
    viewers: '555',
    preview: 'https://static-cdn.jtvnw.net/previews-ttv/live_user_lirik-320x180.jpg',
    url: 'https://www.twitch.tv/lirik'
  }
];

export default Ember.Route.extend({
  model() {
    return new Ember.RSVP.Promise(function(resolve, reject) {
      console.log(Twitch);
      Twitch.init({clientId: ENV.APP.twitchClientId}, function(error, status) {
        // Initialized
      });

      let streams;

      return Twitch.api({method: 'streams', params: {game:'League of Legends', limit:3} }, function(error, list) {
        streams = list.streams.map((stream) => {
          return {
            user: stream.channel.name,
            game: stream.game,
            status: stream.channel.status,
            viewers:stream.viewers,
            preview: stream.preview.medium,
            url: stream.channel.url
          };
        });
        if (error) {
          Ember.run(null, reject, error);
        } else {
          Ember.run(null, resolve, streams);
        }
      });
    });
  }
});
